<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->get('/users', 'UserController@index');
Route::middleware('auth:api')->post('/users/approve/{id}', 'UserController@approve');
Route::middleware('auth:api')->post('/users/disapprove/{id}', 'UserController@disapprove');
Route::middleware('auth:api')->post('/users/delete/{id}', 'UserController@destroy');


Route::get('/blogs', 'BlogController@index');
Route::get('/blog/showBlog/{id}', 'BlogController@showBlog');
Route::get('/home/approveBlog', 'BlogController@allBlogsForApproval');

Route::middleware('auth:api')->post('/blog/store', 'BlogController@store');
Route::middleware('auth:api')->post('/blog/update/{id}', 'BlogController@update');
Route::middleware('auth:api')->post('/blog/destroy/{id}', 'BlogController@destroy');
Route::middleware('auth:api')->post('/blog/approve/{id}', 'BlogController@approve');


Route::get('/comments/{id}', 'CommentController@index');
Route::middleware('auth:api')->post('/comment/store', 'CommentController@store');
Route::middleware('auth:api')->post('/comment/delete/{id}', 'CommentController@destroy');