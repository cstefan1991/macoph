<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index')->name('homePage');
Route::get('/home', 'PagesController@home')->name('home')->middleware(['auth', 'checkUser']);
Route::get('/contact', 'PagesController@contact')->name('contact');
Route::get('/about', 'PagesController@about')->name('about');
Route::get('/pending', 'PagesController@showPendingMessage')->name('pending');
Route::get('/home/approveUser', 'PagesController@approveUser')->name('approveUser');
Route::get('/home/approveBlog', 'PagesController@approveBlog')->name('approveBlog');



Route::get('/newBlog', 'BlogController@create')->name('newBlog');
Route::get('/show/{id}', 'BlogController@show')->name('showBlog');
Route::get('/edit/{id}', 'BlogController@edit')->name('editBlog');

Route::get('/delete/{id}', 'BlogController@destroy')->name('deleteBlog');

Auth::routes();

