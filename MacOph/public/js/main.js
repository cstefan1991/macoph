$(function() {
    
    var ajaxLoading = false;

    function createContent(array) {
        let content = '';
        let status = '';
        let buttons = '';

        array.forEach(user => {
            
            if(user.is_active) {
                status = "<span class='text-success'>Active</span>"
                
                buttons = `            
                    <button data-id="${user.id}" type="submit" class="btn btn-sm btn-outline-primary aprove-delete">Disapprove</button>
                    <button data-id="${user.id}" type="submit" class="btn btn-sm btn-outline-danger aprove-delete">Delete</button>
                `
            } else {
                status = "<span class='text-danger'>Inctive</span>"
                
                buttons = `
                <button data-id="${user.id}" type="submit" class="btn btn-sm btn-outline-success aprove-delete">Approve</button>
                <button data-id="${user.id}" type="submit" class="btn btn-sm btn-outline-danger aprove-delete">Delete</button>
                `
            }

            content += `
            <tr>
                <td>${user.name}</td>
                <td>${user.email}</td>
                <td>${status}</td>
                <td>${user.role.role}</td>
                <td>${buttons}</td>
            </tr>
            `
        });
        return content;
    }
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    var api_token = $('meta[name="api-token"]').attr('content');

    
    data_to_send = {
        'csrf-token': csrf_token,
        'api_token' : api_token
    }

    
    // GET-REQUEST FOR USERS
    $.get('/api/users', data_to_send, function(data) {
        let pendingApprovalUsers = data.filter(user => {
            return !user.is_active;
        });
        let allUsers = data.filter(user => {
            return user.is_active;
        });

        let tableContent = createContent(pendingApprovalUsers);
        $('#tbody-pending-users').html(tableContent);
        
        tableContent = createContent(allUsers);
        $('#tbody-all-users').html(tableContent);
    });



    // POST-REQUEST FOR APPROVAL DISAPPROVAL AND DELETION OF USERS

    
    $(document).on('click', '.aprove-delete', function (e) {
        let typeOfBtn = $(this).text();
        let id = $(this).attr('data-id');
        
        let route = '';
        if(typeOfBtn == "Approve") {
            route = '/api/users/approve/'+id;
        } else if(typeOfBtn == "Disapprove") {
            route = '/api/users/disapprove/'+id;
        } else {
            route = '/api/users/delete/'+id;
        }

        let data_to_send = {
            'csrf-token': csrf_token,
            'api_token' : api_token,
            'id'        : id
        }

        $.post(route, data_to_send, function(data) {
            if(data.message == 'approved') {
                alert('User Approved');
            } else if(data.message == 'disapproved') {
                alert('User Disapproved');
            } else if(data.message == 'deleted') {
                alert('User Deleted');
            }

            let pendingApprovalUsers = data.users.filter(user => {
                return !user.is_active;
            });
            let allUsers = data.users.filter(user => {
                return user.is_active;
            });
    
            let tableContent = createContent(pendingApprovalUsers);
            $('#tbody-pending-users').html(tableContent);
            
            tableContent = createContent(allUsers);
            $('#tbody-all-users').html(tableContent);
        });

    });

    
    
    //ANIMATION JQUERY FOR WELCOME BLADE


    $(document).on('mouseenter', '.animation-overlay', function(e) {
        let button = $(e.target).children().eq(0);
        let post_description = $(e.target).parent().children().eq(0);
        
        $(button).show(250);
        $(post_description).hide(250);

    });

    $(document).on('mouseout', '.animation-overlay', function(e) {
        let button = $(e.target).children().eq(0);
        let post_description = $(e.target).parent().children().eq(0);
        
        let btnWidth = $(e.target).css('width');
        btnWidth = parseInt(btnWidth);

        if( $(button).is(':animated')) {

            $(button).stop(true, true);
            $(post_description).stop(true, true);

        } 
            $(button).hide(250);
            $(post_description).show(250);
        
    });

    $(document).on('mouseenter', '.btn-show-post', function(e) {
        let text = $(e.target).parent().parent().children().eq(0);
        let btnWidth = $(e.target).css('width');
        btnWidth = parseInt(btnWidth);

        if(btnWidth < 70) {
            return;
        }

        $(e.target).stop();
        $(text).hide();
    })


    $(document).on('mouseover', '.side-nav-link', function(){
        let sideItem = $(this).parent()[0].id
        $("#"+sideItem).animate({ marginLeft: '10%' }, 200);
    })

      $(document).on('mouseout', '.side-nav-link', function(){
        let sideItem = $(this).parent()[0].id
        $("#"+sideItem).animate({ marginLeft: '0' }, 200);
    })


    // POST REQUEST FOR ADD-NEW-POST

    $(document).on('click', '#save', function(e) {
        e.preventDefault();

        $('.main_img_input').css({'border':'1px solid #ced4da'});
        $('.title_input').css({'border':'1px solid #ced4da'});
        $('.simditor-wrapper').css({'border':'1px solid #ced4da'});
        $('.simditor-body').removeAttr('data-text');


        let data_to_send = {
            "csrf_token" : csrf_token,
            "api_token"  : api_token,
            "title"      : $('.title_input').val(),
            "main_image" : $('.main_img_input').val(),
            "category"   : $('.category-input').val(),
            "body"       : $('.simditor-body').html()
        }


        $.post('/api/blog/store', data_to_send, function(data) {
            if(data.success) {
                $('.blog-msgs').removeClass('alert alert-danger')
                    .addClass('alert alert-success').text(data.msg).slideDown();
            } else {
                $('.blog-msgs').removeClass('alert alert-success')
                    .addClass('alert alert-danger').text(data.error).slideDown();
            }
        }).then(function(data) {
            $('.title_input').val('')
            $('.main_img_input').val('')
            $('.simditor-body').html('')
        })
        .fail(function(error){
            $.each(error.responseJSON.errors, function(err, msg){
                console.log(err)
                console.log(msg)
                if(err == 'main_image') {
                    $('.main-image').val('')
                    $('.main_img_input')
                    .attr('placeholder', msg)
                    .css({'border':'2px solid #A64452', 'opacity':'0.8'});
                }
                else if(err == 'title') {
                    $('.title_input').val('')
                    $('.title_input')
                    .attr('placeholder', msg)
                    .css({'border':'2px solid #A64452', 'opacity':'0.8'});

                }
                else if(err == 'body') {
                    $('.simditor-wrapper').css({'border':'2px solid #A64452', 'opacity':'0.8'});
                    let simditorBodyTxtLength = $('.simditor-body').text().length;
                    if(simditorBodyTxtLength < 12) {
                        $('.simditor-body').text('')
                        $('.simditor-body').attr('data-text', msg)
                        .css({'color':'#A64452', 'opacity':'0.8', 'font-size':'0.9em'});
                    }
                } 
            });
        });
        
    });

    // POST REQUEST FOR EDIT-POST

    $(document).on('click', '#update', function(e) {
        let blogId = $('#update').attr('data-id');
        let data_to_send = {
            "csrf_token" : csrf_token,
            "api_token"  : api_token,
            "title"      : $('.title_input').val(),
            "main_image" : $('.main_img_input').val(),
            "category"   : $('.category-input').val(),
            "body"       : $('.simditor-body').html()
        }

        $.post('/api/blog/update/' + blogId, data_to_send, function(data) {
            console.log(data);
        }).then(function(data){
            
        });

    }); // on click update


    // table for listing all unApproved blogs
    function createTable(data){
        let html = '';

        data.forEach(blog => {
            let buttons = ''

            if(blog.is_approved == 0) {
                blog.is_approved = "Неодобрен"
            }

            buttons = (`
                <button class="btn btn-default btn-sm btnApprove" data-id="${blog.id}">Одобри</button>
                <button class="btn btn-default btn-sm btnShow">
                    <a href="/show/${blog.id}">Прикажи</a>
                </button>
                <button class="btn btn-default btn-sm btnEdit" data-id="${blog.id}">
                    <a href="/edit/${blog.id}">Едитирај</a>
                </button>
                <button class="btn btn-default btn-sm btnDestroy" data-id="${blog.id}">Избриши</button>
            `);

            html += (`
                <tr>
                    <td><img src="${blog.main_image}"></td>
                    <td>${blog.user.name}</td>
                    <td>${blog.title}</td>
                    <td>${blog.category_id}</td>
                    <td>${blog.is_approved}</td>
                    <td class="btnActionsTd"> ${buttons} </td>
                </tr>
            `);

        });

        $('.listBlogsBodyTable').append(html);
        
    } // create table function

    $.get('/api/home/approveBlog', function(data){
        createTable(data)
    });

    $(document).on('click', '.btnDestroy', function(){
        let id = $(this).attr('data-id');
        $('.listBlogsBodyTable').html('');
        
        $.post(`/api/blog/destroy/${id}`, data_to_send, function(data){
            if(!ajaxLoading) {
                ajaxLoading = true;
                $.get('/api/home/approveBlog', function(data){
                    ajaxLoading = false;
                    createTable(data)
                });
            }
        }) // post request
    }); // btnDestroy on click



    $(document).on('click', '.btnApprove', function(e) {
        e.preventDefault();
        let id = $(this).attr('data-id');
        $('.listBlogsBodyTable').html('');

        $.post(`/api/blog/approve/${id}`, data_to_send, function(data){

            if(!ajaxLoading) {
                ajaxLoading = true;
                $.get('/api/home/approveBlog', function(data){
                    ajaxLoading = false;
                    createTable(data)
                });
            };
            
        });
    });

}); // document ready