@extends('layouts.app')


@section('content')
<div class="jumbotron">
    <h3 class="jumbotron-heading">Ве молиме почекате за одобрување.</h3> 
    <p>Откако ќе бидете одобрени ќе добиете е-мејл и пристап до панелот за додавање блогови.</p>
    <h5 class="text-right">Ви благодариме.</h5>   
</div>    
@endsection