@extends('layouts.app')

@section('content')

<div class="row">
    
    <div class="col-md-3 side-title-container p-0">
            <a href="{{route('homePage')}}">
                <i class="fa fa-arrow-circle-left"></i>
            </a>
            <div class="side-category-underline"></div>
            <div class="likeAndSaveContainer">
                @auth
                    @if (auth()->user()->type_of_user_id != 1)                         
                    <i class="fa fa-thumbs-up like_blog" data-id="{{$id}}"></i>
                    <i class="fa fa-save save_blog" data-id="{{$id}}"></i>
                    @endif
                    @if (auth()->user()->type_of_user_id == 1)
                    
                    <a href="{{route('deleteBlog', $id)}}">
                        <i class="fa fa-trash-o delete_blog"></i>  
                    </a>
                    <a class="edit_blog" href="{{route('editBlog', $id)}}">
                        <i class="fa fa-pencil-square-o"></i>                  
                    </a>
                    @endif
                @endauth
            </div>
    </div>

    <div class="col-md-9 offset-md-3" id="mainShowContent">

    </div> {{-- main show content --}}
    <div class="col-md-7 offset-md-4 blog-body">

    </div>
</div>

<div class="row">
    <div class="parent-comments-container">
        <div class="col-md-9 offset-md-3">
            
            <div class="row">
                @auth
                <div class="col-md-4 textarea-container">
                    <input id="comment-text-area" class="form-control" name="" cols="30" rows="10" placeholder="Напиши коментар . . .">
                    <span class="search-input-underline"></span>
                </div>
                @endauth
                <div class="{{auth()->check() ? 'col-md-8' : 'col-md-12'}} comments-col">

                </div>

            </div>

        </div>
    </div>
</div>
            
    



<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
<script>
    $(function(){

        if(location.href == 'http://127.0.0.1:8000/show/' + {{$id}}) {
            $('body').css('padding-top','0' );
        }


        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        var api_token = $('meta[name="api-token"]').attr('content');
    
    
        let data_to_send = {
            'csrf-token': csrf_token,
            'api_token' : api_token
        }
        
        // GET REQUEST FOR THE CONTENT OF THE BLOG

        $.get('/api/blog/showBlog/' + {{$id}}, data_to_send, function(data) {
            $('#title').html(data.title);
            $('#mainShowContent').css('background-image', 'url('+data.main_image+')')
        
            $('.blog-body').append(data.body);

            let userId = $('meta[name="user_id"]').attr('content');
            
            if( userId != data.user_id) {
                $('.edit_blog').hide();
            }
        });

        // FUNCTION FOR PRINTIG THE COMMENTS 

        function printComments(array) {
            let html = "";
            let deleteBtn = ``;

            for(let i = 0; i < array.length; i++) {
                
                if(array[i].user_id == $('meta[name="user_id"]').attr('content') || $('meta[name="type_of_user"]').attr('content') == 1) {
                    deleteBtn = `<i data-id="${array[i].id}" class="fa fa-trash-o deleteCommentBtn"></i>`
                } else {
                    deleteBtn = ``;
                }
                html += `
                <div class="user-coments-container p-3 mt-2">
                    <p class="comment">${array[i].comment}</p>
                    <footer class="pull-right blockquote-footer p-0 m-0 text-right text-dark">${array[i].user.name}<br><cite title="Time">${array[i].created_at}</cite></footer>
                    <div class="clear-fix comment-action">
                        ${deleteBtn}
                    </div>
                </div>
                `
            }
            return html;
        }

        // GET REQUEST FOR THE COMMENTS

        $.get('/api/comments/' + {{$id}}, function(data) {
              $('.comments-col').html(printComments(data));
        });

        // POST REQUEST FOR STORING NEW COMMENTS

        $(document).on('keypress', '#comment-text-area', function(e) {
            if(e.which == 13 && $(e.target).val() != '') {
                let data_to_send = {
                    'csrf-token': csrf_token,
                    'api_token' : api_token,
                    'comment'   : $(e.target).val(),
                    'blog_id'   : {{$id}}
                }

                $(e.target).val("");

                $.post('/api/comment/store', data_to_send, function(data) {
                    $('.comments-col').html(printComments(data.comments));

                });
            }  

        });

        $(document).on('click', '.deleteCommentBtn', function(e) {
            let comentId = $(e.target).attr('data-id');
            $(e.target).parent().parent().hide(600)
            $.post('/api/comment/delete/' + comentId, data_to_send, function(data) {
                $('.comments-col').html(printComments(data.comments));
            })
        })

    });
</script>
@endsection