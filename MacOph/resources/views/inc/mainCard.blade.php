<div class="row">

    <div class="col-md-8 p-0">
        <div class="card-body">
            <img class="mainPostImg1" src="https://previews.123rf.com/images/dolgachov/dolgachov1412/dolgachov141203347/34710495-medicine-eyesight-control-laser-correction-people-and-health-concept-beautiful-young-woman-pointing-.jpg" alt="">
            
            <div class="main-post-description">
                <p class="main-post-title">Наслов Наслов Наслов Наслов Наслов Наслов Наслов</p>
                <p class="main-post-category">Категорија</p>
            </div>
            <div id="overlay">

            </div>
            <div class="animation-overlay">
                <button class="btn btn-show-post">Повеќе...</button>
            </div>
        </div>
    </div>
    
    <div class="col-md-4">
        <div class="row">
            
            <div class="col-md-12 p-0">
                <div class="card-body mainImg2">
                    <img class="mainPostImg2" src="https://theophthalmologist.com/fileadmin/_processed_/9/1/csm_0615-301-Main-Iris_008a358389.png" alt="">
                    
                    <div class="post-description">
                        <p class="post-title">Наслов Наслов Наслов Наслов Наслов Наслов Наслов</p>
                        <p class="post-category">Категорија</p>
                    </div>
                    <div id="overlay">

                    </div>
                    <div class="animation-overlay">
                        <button style="font-size:1em;" class="btn btn-show-post">Повеќе...</button>
                    </div>
                </div>
            </div>    

            <div class="col-md-12 p-0">

                <div class="card-body mainImg3">
                    <img class="mainPostImg3" src="https://theophthalmologist.com/fileadmin/_processed_/d/4/csm_0615-301-appointments_2593f6d488.png" alt="">
                    
                    <div class="post-description">
                        <p class="post-title">Наслов Наслов Наслов Наслов Наслов Наслов Наслов</p>
                        <p class="post-category">Категорија</p>
                    </div>
                    <div id="overlay">
                
                    </div>
                    <div class="animation-overlay">
                        <button style="font-size:1em;" class="btn btn-show-post">Повеќе...</button>
                    </div>
                </div>
            </div>    

        </div>
    </div>
        
</div>