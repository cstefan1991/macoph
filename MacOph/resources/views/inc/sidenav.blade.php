
<ul class="side-nav">
    <li class="side-nav-item my-4" id="sideNav1">
        <a class="side-nav-link" href="{{route('homePage')}}">Home</a>
    </li>
    <li class="side-nav-item my-4" id="sideNav2">
        <a class="side-nav-link" data-toggle="collapse" href="#categories" aria-expanded="false" aria-controls="categories">Categories</a>
        <ul id="categories" class="collapse">
            <li class="side-nav-item my-4">
               <a href="" class="side-nav-link">Category 1</a>
            </li>
            <li class="side-nav-item my-4">
                <a href="" class="side-nav-link">Category 2</a>
             </li>
             <li class="side-nav-item my-4">
                <a href="" class="side-nav-link">Category 3</a>
             </li>
        </ul>
    </li>
    <li class="side-nav-item my-4" id="sideNav3">
        <a class="side-nav-link" href="{{route('about')}}">About</a>
    </li>
    <li class="side-nav-item my-4" id="sideNav4">
        <a class="side-nav-link" href="{{route('contact')}}">Contact</a>
    </li>
</ul>
