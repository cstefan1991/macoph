@extends('layouts.app')


@section('content')

<div class="row" id="main_cards">

</div>
<div class="row mt-4 mb-4">
    <div class="col-md-10">
        <div class="row" id="small_cards">
            
        </div>
    </div>
</div>


    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
    <script>
        $(function(){

                // GET-REQUEST FOR BLOGS

    $.get('/api/blogs', function(data) {
        let main_cards = ``;
        let small_cards = ``;
        for(let i = 0; i < data.length; i++) {
            
            let blog = data[i];

            if(i == 0) {

                main_cards += `
                <div class="col-md-6 p-0 mainPostImg1" style="background-image:url('${blog.main_image}')">
                    <div class="main-post-description">
                        <p class="main-post-title">${blog.title}</p>
                        <p class="main-post-category">Категорија</p>
                        <footer class="blockquote-footer p-0 m-0 text-right text-white">${blog.user.name}<br><cite title="Time">${blog.created_at}</cite></footer>
                    </div>
                    <div id="overlay">

                    </div>
                    <div class="animation-overlay">
                        <a href="/show/${blog.id}"
                        <button class="btn btn-show-post">Повеќе...</button>
                        </a>
                    </div>
                </div>
                `
            } else if(i == 1) {
                main_cards += `
                <div class="col-md-4">
                    <div class="row">
                        
                        <div class="col-md-12 p-0 mainPostImg2" style="background-image:url('${blog.main_image}')" >
                                
                            <div class="post-description">
                                <p class="post-title">${blog.title}</p>
                                <p class="post-category">Категорија</p>
                                <footer class="blockquote-footer p-0 m-0 text-right text-white">${blog.user.name}<br><cite title="Time">${blog.created_at}</cite></footer>
                            </div>
                            <div id="overlay">

                            </div>
                            <div class="animation-overlay">
                                <a href="/show/${blog.id}"
                                <button style="font-size:1em;" class="btn btn-show-post">Повеќе...</button>
                                </a>
                            </div>
                            
                        </div>
                
                `
            } else if(i == 2) {
                main_cards += `
                        <div class="col-md-12 p-0 mainPostImg3" style="background-image:url('${blog.main_image}')">
                            
                            <div class="post-description">
                                <p class="post-title">${blog.title}</p>
                                <p class="post-category">Категорија</p>
                                <footer class="blockquote-footer p-0 m-0 text-right text-white">${blog.user.name}<br><cite title="Time">${blog.created_at}</cite></footer>
                            </div>
                            <div id="overlay"></div>
                            <div class="animation-overlay">
                                <a href="/show/${blog.id}"
                                <button style="font-size:1em;" class="btn btn-show-post">Повеќе...</button>
                                </a>
                            </div>
                        </div>
                       
                    </div>
                </div>

                `
            } else {
                small_cards += `
                <div class="col-md-4 card-layout p-0 mainImg" style="background-image:url('${blog.main_image}')">

                    <div class="post-description">
                        <p class="post-title">${blog.title}</p>
                        <p class="post-category">Категорија</p>
                        <footer class="blockquote-footer p-0 m-0 text-right text-white">${blog.user.name}<br><cite title="Time">${blog.created_at}</cite></footer>
                    </div>

                    <div id="overlay"></div>
                    <div class="animation-overlay">
                        <a href="/show/${blog.id}"
                        <button style="font-size:1em;" class="btn btn-show-post">Повеќе...</button>
                        </a>
                    </div>
                </div>
                `
            }

        }
        $('#main_cards').hide();
        $('#small_cards').hide();

        $('#main_cards').html(main_cards);
        $('#small_cards').html(small_cards);

    }).then(function(data) {
        $('#main_cards').show(1000);
        $('#small_cards').show(1000);

    })

        }) // document ready
    </script>
@endsection