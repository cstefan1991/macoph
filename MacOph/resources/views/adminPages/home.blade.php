@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-4">
            <a href="{{route('approveUser')}}">
                <div class="card">
                    <div class="card-body">
                        <p class="text-center">Менаџирање со корисници</p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-4">
            <a href="{{route('approveBlog')}}">
                <div class="card">
                    <div class="card-body">
                        <p class="text-center">Менаџирање со блогови</p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-4">
            <a href="{{route('newBlog')}}">
                <div class="card">
                    <div class="card-body">
                        <p class="text-center">Додади нов блог</p>
                    </div>
                </div>
            </a>
        </div>
    </div>
@endsection