@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-8 offset-md-2">
            <h3 class="text-center">Нов Блог</h3>
            <form id="new_blog_form" action="" method="post">

                <div class="form-group">
                    <label for="">Изберете категорија</label>
                    <select class="form-control category-input" name="" id="">
                        <option value="1">Category</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="">Главна слика</label>
                    <input type="url" class="form-control main_img_input" required>
                </div>

                <div class="form-group">
                    <label for="">Наслов</label>
                    <input type="text" class="form-control title_input" required>
                </div>

                <div class="form-group">
                    <label for="paragraph">Параграф</label>
                    <textarea id="editor" cols="30" rows="10" class="form-control paragraph" required></textarea>
                </div>

                <div class="blog-msgs"></div>

                <button type="submit" class="btn btn-outline-dark btn-block mb-4" id="save">Зачувај</button>
            </form>
        </div>

    </div>

    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
                            
    <script>
        $(function(){
            var editor = new Simditor({
            textarea: $('#editor')
            //optional options
            });
    
        }) // document ready
    </script>
@endsection