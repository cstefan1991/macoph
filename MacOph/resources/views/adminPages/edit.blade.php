@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-8 offset-md-2">
            <h3 class="text-center">Коригирање на Блог</h3>
            <div class="form-group">
                <label for="">Изберете категорија</label>
                <select class="form-control category-input" name="" id="">
                    <option value="1">Category</option>
                </select>
            </div>
            <div class="form-group">
                <label for="">Главна слика</label>
                <input type="url" class="form-control main_img_input" value="{{$blog->main_image}}">
            </div>
            <div class="form-group">
                <label for="">Наслов</label>
                <input type="text" class="form-control title_input" value="{{$blog->title}}">
            </div>
            <div class="form-group">
                    <label for="paragraph">Параграф</label>
                    <textarea id="editor" cols="30" rows="10" class="form-control paragraph"></textarea>
                </div>
                <button type="submit" data-id="{{$blog->id}}" class="btn btn-outline-dark btn-block mb-4" id="update">Зачувај</button>
        </div>
    </div>

    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
                            
    <script>
        $(function(){
        
            var editor = new Simditor({
            textarea: $('#editor')
            //optional options
            });
            let body = `{!! $blog->body !!}`
            $('.simditor-body').html(body);
        }) // document ready
    </script>
@endsection