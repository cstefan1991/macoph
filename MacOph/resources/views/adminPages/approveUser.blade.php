@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-6">
        <h3 class="text-center">Pending Approval</h3>
        <table class="table table-sm">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Type of User</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody id="tbody-pending-users">
                
            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <h3 class="text-center">All Users</h3>
        <table class="table table-sm">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Type of User</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody id="tbody-all-users">

            </tbody>
        </table>
    </div>
</div>
@endsection
