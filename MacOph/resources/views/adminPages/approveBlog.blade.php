@extends('layouts.app')

@section('content')

<table class="table unApprovedBlogsTable">
    <thead>
      <tr>
        <th scope="col">Слика</th>
        <th scope="col">Корисник</th>
        <th scope="col">Наслов</th>
        <th scope="col">Категорија</th>
        <th scope="col">Неодобрени</th>
        <th scope="col">Акции</th>
      </tr>
    </thead>
    <tbody class="listBlogsBodyTable">

    </tbody>
  </table>
@endsection