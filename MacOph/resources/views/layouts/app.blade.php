<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @auth
        <meta name="api-token" content="{{ auth()->user()->api_token }}"> 
        <meta name="user_id" content="{{ auth()->user()->id }}">    
        <meta name="type_of_user" content="{{ auth()->user()->type_of_user_id }}">    
    @endauth

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('js/simditor-2.3.28/styles/simditor.css') }}" rel="stylesheet">


</head>
<body>
    <div id="app">
        @if (isset($id))
            @if (\Request::url() !== route('showBlog', $id))
                @include('inc.nav')
            @endif    
        @else
            @include('inc.nav')
        @endif
        <main class="container-fluid">
            @if (\Request::url() == route('homePage') || \Request::url() == route('about') || \Request::url() == route('contact'))
            <div class="row">
                <div id="side-nav-container" class="col-md-2">
                    @include('inc.sidenav')
                </div>
            </div>
            @endif
    
            <div class="row">
                <div class="{{\Request::url() == route('homePage') || \Request::url() == route('about') || \Request::url() == route('contact') ? 'col-md-9 offset-md-3' : 'col-md-12' }}">
                    @yield('content')
                </div>
            </div>
    
        </main>
        {{-- @include('inc.footer') --}}
    </div>


    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/simditor-2.3.28/site/assets/scripts/module.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/simditor-2.3.28/site/assets/scripts/hotkeys.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/simditor-2.3.28/site/assets/scripts/uploader.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/simditor-2.3.28/site/assets/scripts/simditor.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>

</body>
</html>
