<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Maja Ivanova',
            'email' => 'maja_ivanova90@yahoo.com',
            'password' => Hash::make('password'),
            'type_of_user_id' => 1,
            'api_token' => Str::random(80),
            'subscribe' => 0
        ]);
    }
}
