<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('type_of_user_id', '>', 1)->orderBy('created_at', 'desc')->get();
        return response()->json($users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function approve($id) {
        $user = User::find($id);
        $user->is_active = 1;
        if($user->save()) {
            return response()->json([
                'users' => User::where('type_of_user_id', '>', 1)->orderBy('created_at', 'desc')->get(),
                'message' => 'approved']);
        }
    }

    
    public function disapprove($id) {
        $user = User::find($id);
        $user->is_active = 0;
        if($user->save()) {
            return response()->json([
                'users' => User::where('type_of_user_id', '>', 1)->orderBy('created_at', 'desc')->get(),
                'message' => 'disapproved']);
        }
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function destroy($id)
    {
        $user = User::find($id);
        if($user->delete()) {
            return response()->json([
                'users' => User::where('type_of_user_id', '>', 1)->orderBy('created_at', 'desc')->get(),
                'message' => 'deleted']);
        }
    }
}
