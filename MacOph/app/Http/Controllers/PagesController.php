<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    
    // Available for anyone
    public function index() {
        return view('welcome');
    }
    public function about() {
        return view('about');
    }
    public function contact() {
        return view('contact');
    }


    // Available only for admin
    public function home() {
        return view('adminPages.home');
    }

    public function approveUser() {
        return view('adminPages.approveUser');
    }
    public function approveBlog() {
        return view('adminPages.approveBlog');
    }

    // Available only if ediotr is not approved jet
    public function showPendingMessage() {
        return view('editorPages.pendingApproval');
    }

}
