<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\BlogRequest;
use Illuminate\Contracts\Validation\Validator;
use App\Blog;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::where('is_approved', '1')->orderBy('created_at', 'desc')->get();
        return response()->json($blogs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adminPages.newBlog');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogRequest $request)
    {
        
        //Не вака валидација, треба да направиш „php aritsan make:request BlogRequest“
        // и после во заградите на store ќе напишеш BlogRequest $request, а валидациите
        // ќе си ги направиш во тој фајл што ќе се генерира наместо овде. Контролерот мора да биде чист.
        // И не мора да ги враќаш преку json, 
        //самиот ларавел треба да ги враќа преку json ако има ерори, ти треба само да ги напишеш валидациите.
        
        $rules = [
            'category' => 'required',
            'main_image' => 'required',
            'title' => 'required',
            'body' => 'min:12'
        ];
        
        $blog = new Blog;
        $blog->user_id = auth()->user()->id;
        $blog->category_id = $request->category;
        $blog->main_image = $request->main_image;
        $blog->title = $request->title;
        $blog->body = $request->body;
        if(auth()->user()->type_of_user_id == 1) {
            $blog->is_approved = 1;
        }
        
        if($blog->save()) {
            return response()->json(['success' => 1, 'msg' => 'Успешно додадовте блог.']);
        }
        return response()->json(['error' => 'Неуспешно!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('show', ['id' => $id]);
    }

    public function showBlog($id) {
        
        $blog = Blog::find($id);
        return response()->json($blog);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::find($id);

        return view('adminPages.edit', ['blog' => $blog]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BlogRequest $request, $id)
    {
        $blog = Blog::find($id);

        $blog->user_id = auth()->user()->id;
        $blog->title = $request->title;
        $blog->main_image = $request->main_image;
        $blog->category_id = $request->category;
        $blog->body = $request->body;
        
        if($blog->save()) {
            return response()->json(['message' => 'success']);
        }
        return response()->json(['message' => 'error']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // Checks if the request is Api and returns a JSON
        if($request->is('blog/destroy/'.$id)) {
            if(Blog::destroy($id)) {
                return response()->json(['message' => 'success']);
            }
    
            return response()->json(['message' => 'error']);
        } 
        // Checks if the request is Web redirects to homepage
        else {
            Blog::destroy($id);
            return redirect()->route('homePage');
        }
    }

    public function approve($id)
    {   
        $blog = Blog::find($id);
        $blog->is_approved = 1;

        
        if($blog->save()) {
            return response()->json(['message' => 'success']);
        }

        return response()->json(['message' => 'error']);
    }

    //send all blogs to approveBlog blade
    public function allBlogsForApproval()
    {
        $blogs = Blog::where('is_approved', '0')->get();
        return response()->json($blogs);
    }
}
