<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category' => 'required',
            'main_image' => 'required|url',
            'title' => 'min:3',
            'body' => 'min:12'
        ];
    }

    public function messages()
    {
        return [
            'category.required' => 'ПОЛЕТО Е ЗАДОЛЖИТЕЛНО !!',
            'main_image.required' => 'ПОЛЕТО Е ЗАДОЛЖИТЕЛНО !!',
            'main_image.url' => 'ПОЛЕТО ТРЕБА ДА БИДЕ ВАЛИДНО `URL` !!',
            'title.min' => 'ПОЛЕТО Е ЗАДОЛЖИТЕЛНО И ТРЕБА ДА ИМА НАЈМАЛКУ 3 КАРАКТЕРИ !!',
            'body.min' => 'ПОЛЕТО Е ЗАДОЛЖИТЕЛНО И ТРЕБА ДА ИМА НАЈМАЛКУ 5 КАРАКТЕРИ !!',
        ];
    }

}
