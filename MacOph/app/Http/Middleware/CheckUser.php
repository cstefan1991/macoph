<?php

namespace App\Http\Middleware;

use Closure;

class CheckUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->type_of_user_id == 1) {
            return $next($request);
        } else if(auth()->user()->type_of_user_id == 2) {
            
            if(auth()->user()->is_active) {
                return redirect()->route('newBlog');
            } else {
                return redirect()->route('pending');
            }

        } else {
            return redirect()->route('homePage');
        }
    }
}
