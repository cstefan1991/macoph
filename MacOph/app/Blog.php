<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = ['user_id', 'main_image', 'title', 'category_id', 'body'];
    
    protected $with = ['user', 'comments'];
    
    public function user() {
        return $this->belongsTo(User::class);
    }

    public function category() {
        return $this->belognsTo(Category::class);
    }

    public function comments() {
        return $this->hasMany(Comment::class);
    }

    public function likes() {
        return $this->hasMany(Like::class);
    }


    public function savedByUsers() {
        return $this->hasMany(SavedBlog::class);
    }
}
